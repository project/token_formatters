<?php

namespace Drupal\token_formatters\Plugin\Field\FieldFormatter;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;
use Drupal\token\TokenEntityMapperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation for 'token_formatters_entity_reference'.
 *
 * @FieldFormatter(
 *   id = "token_formatters_entity_reference",
 *   label = @Translation("Tokenized text"),
 *   description = @Translation("Display tokenized text as an optional link with tokenized destination."),
 *   field_types = {
 *     "entity_reference",
 *     "entity_reference_revisions"
 *   }
 * )
 */
class EntityReferenceTokenFormatter extends EntityReferenceFormatterBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The token entity mapper.
   *
   * @var \Drupal\token\TokenEntityMapperInterface
   */
  protected $tokenEntityMapper;

  /**
   * Constructs an EntityReferenceTokenFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   * @param \Drupal\token\TokenEntityMapperInterface $token_entity_mapper
   *   The token entity mapper.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, ModuleHandlerInterface $module_handler, Token $token, TokenEntityMapperInterface $token_entity_mapper) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings,
    );

    $this->moduleHandler = $module_handler;
    $this->token = $token;
    $this->tokenEntityMapper = $token_entity_mapper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('module_handler'),
      $container->get('token'),
      $container->get('token.entity_mapper'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'text' => '[entity:label]',
      'link' => '[entity:url]',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text to output'),
      '#description' => $this->t('The text to display for this field. You may include HTML. This field accepts tokens.'),
      '#default_value' => $this->getSetting('text'),
      '#required' => TRUE,
    ];

    $elements['link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link destination'),
      '#description' => $this->t('Leave blank to output the text only not as a link. This field accepts tokens.'),
      '#default_value' => $this->getSetting('link'),
    ];

    if ($this->moduleHandler->moduleExists('token')) {
      $entity_type = $this->getFieldSetting('target_type');
      $token_type = $this->tokenEntityMapper->getTokenTypeForEntityType($entity_type);
      $validate = [
        '#element_validate' => ['token_element_validate'],
        '#token_types' => [$token_type, 'entity'],
      ];
      $elements['link'] += $validate;
      $elements['text'] += $validate;

      $elements['tokens'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => [$token_type, 'entity'],
        '#global_types' => FALSE,
      ];
    }

    return $elements
      + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary[] = $this->t('Text: %text', ['%text' => $this->getSetting('text')]);
    if ($link = $this->getSetting('link')) {
      $summary[] = $this->t('Linked to: %link', ['%link' => $link]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $entity_type = $this->getFieldSetting('target_type');
    $token_type = $this->tokenEntityMapper->getTokenTypeForEntityType($entity_type);

    $token_options = ['clear' => TRUE];
    $text_setting = $this->getSetting('text');
    $link_setting = $this->getSetting('link');

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $item) {
      $token_data = [
        $token_type => $item,
        'entity' => $item,
      ];
      $text = $this->token->replace($text_setting, $token_data, $token_options);
      if ($link = $this->token->replace($link_setting, $token_data, $token_options)) {
        $linkObj = Link::fromTextAndUrl(Markup::create($text), Url::fromUri($link));
        $elements[$delta] = $linkObj->toRenderable();
      }
      else {
        $elements[$delta] = ['#markup' => $text];
      }
    }

    return $elements;
  }

}
